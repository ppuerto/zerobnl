port_pub_sub = 5556
port_push_pull = 5557

UNIT = {"seconds": 1, "minutes": 60, "hours": 3600}

TEMP_FOLDER = "TMP_FOLDER"
INIT_VALUES_FILE = "init_values.json"
ATTRIBUTE_FILE = "node_attributes.json"

ORCH_FOLDER = "orch"
SEQUENCE_FILE = "sequence.json"
STEPS_FILE = "steps.json"

DOCKERFILE_FOLDER = "Dockerfiles"
DOCKER_COMPOSE_FILE = "docker-compose.yml"

START = "2000-01-01 00:00:00"

DOCKER_HOST = "172.17.01"
REDIS_PORT = 6379
